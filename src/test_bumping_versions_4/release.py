"""Release information for ska-sdp-lmc package."""

NAME = "test-bumping-versions"
VERSION = "0.0.0"
VERSION_INFO = VERSION.split(".")
AUTHOR = "Gabriella Hodosan"
